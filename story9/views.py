from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.contrib.auth.forms import AuthenticationForm
from django.contrib.auth import login, logout
from django.contrib.auth.decorators import login_required


# Create your views here.
@login_required(login_url='story9:login')
def story9(request):
    return render(request, 'homepage.html')

def login_view(request):
    text = ""
    if request.method == "POST":
        form = AuthenticationForm(data = request.POST)
        if form.is_valid():
            user = form.get_user()
            login(request, user)
            return redirect('story9:homepage')
        else:
            text = "Oops, username or password is invalid!"
    else:
        if request.user.is_authenticated:
            return redirect('story9:homepage')
        else:
            form = AuthenticationForm()
    return render(request, 'login.html', {'form': form, 'text':text})

def logout_view(request):
    logout(request)
    return redirect('story9:login')