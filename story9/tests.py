from django.test import TestCase, Client
from django.contrib.auth.models import User
from django.urls import reverse
from django.contrib.auth import login

# Create your tests here.
class Story9Test(TestCase):
    def test_login_url_is_exist(self):
        response = Client().get('/login')
        self.assertEqual(response.status_code, 301)

    def test_homepage_html_is_used(self):
        user = User.objects.create(username="cookiesmonster")
        user.set_password('dont')
        user.save()
        data = {'username': 'cookiesmonster', 'password': 'dont'}

        response = Client().post(reverse('story9:login'), data=data)
        response = Client().get('')
        self.assertEqual(response.status_code, 302)
        self.assertTemplateUsed('homepage.html')

    def test_login_html_is_used(self):
        response = Client().get('/login')
        self.assertEqual(response.status_code, 301)
        self.assertTemplateUsed('login.html')

    def test_login(self):
        user = User.objects.create(username="cookiesmonster")
        user.set_password('dont')
        user.save()
        data = {'username':'cookiesmonster', 'password':'dont'}

        response = Client().post('/login', data=data)

        responseNext = Client().get('')
        self.assertEqual(responseNext.status_code, 302)

    def test_home_url_does_not_logged_in(self):
        response = Client().get('')
        self.assertEqual(response.status_code, 302)

    def test_logout_url_redirect(self):
        response = Client().get(reverse('story9:logout'))
        self.assertEqual(response.status_code, 302)